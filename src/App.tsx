import "bootstrap/dist/css/bootstrap.css";
import { Component, Fragment } from "react";
import { Example } from "./components/Example";
import { KevinComponent } from "./components/KevinComponent";
import { IAppState } from "./models/IAppState";

class App extends Component<any, IAppState> {
    constructor(props: any) {
        super(props);

        this.state = {
            totalRequestsMade: 0
        };

        this.incrementTotalRequestsMade = this.incrementTotalRequestsMade.bind(this);
    }

    incrementTotalRequestsMade(): void {
        this.setState({ totalRequestsMade: this.state.totalRequestsMade + 1 });
    }

    render(): JSX.Element {
        return (
            <Fragment>
                <h1>Components will go below here</h1>
                <div>Total requests made: {this.state.totalRequestsMade}</div>
                <Example
                    incrementTotalRequests={this.incrementTotalRequestsMade}
                />
                <KevinComponent />
            </Fragment>
        );
    }
}

export { App };