import { Component, Fragment } from "react";

class KevinComponent extends Component{
    render(): JSX.Element {
        return (
            <Fragment>
                <h2>Kevin's Component</h2>
                <p>{(new Date()).toString()}</p>
                <iframe width="420" height="315" src="https://www.youtube.com/embed/qTTOWu4AqL8"></iframe> 
            </Fragment>
        );
    }
}
export { KevinComponent };
