import { Component, Fragment } from "react";
import { IExampleProps } from "../models/IExampleProps";
import { IExampleState } from "../models/IExamplesState";
import axios, { AxiosResponse } from "axios";
import { Button } from "reactstrap";

// I happen to know how the response is formatted, so I'm being extremely explicit here.
interface IJokeResponse {
    id: number;
    type: string;
    setup: string;
    punchline: string;
}

/**
 * Press button, get joke.
 */
class Example extends Component<IExampleProps, IExampleState> {
    constructor(props: IExampleProps) {
        super(props);

        this.state = {
            setup: "",
            punchline: ""
        };

        this.getJoke = this.getJoke.bind(this);
    }

    getJoke(): void {
        // Axios requests are asynchronous and return a promise!
        axios({
            method: "GET",
            url: "https://official-joke-api.appspot.com/jokes/random"
        })
            .then((response: AxiosResponse<IJokeResponse>) => {
                this.setState({
                    setup: response.data.setup,
                    punchline: response.data.punchline
                })
            })
            .catch((err: Error) => {
                console.error(err);
            })
            .finally(() => {
                this.props.incrementTotalRequests();
            });
    }

    render(): JSX.Element {
        return (
            <Fragment>
                <h2>Example component</h2>
                <Button
                    onClick={this.getJoke}
                >
                    Get joke
                </Button>
                <div>{this.state.setup}</div>
                <div>{this.state.punchline}</div>
            </Fragment>
        );
    }
}

export { Example };