# Assignment 2
Code reviews, merging, passing information between parent and child, RESTful requests?

## Goals For This Assignment
- Code reviews
  - Reviewing / responding
- Potentially handling merge conflicts when merging into the main branch?
- Understand the hierarchy of parent and child component
  - What can be passed to who.
  - How to pass information from parent -> child.
  - How to pass information from child -> parent.
- Basics of RESTful requests
  - Using Axios to retrieve information from an API.

## Information
I'm trying to tackle a lot of final-project-related things from here on out.

### Code Reviews And Merging
Ideally, whenever we want to make changes to our code, we work in a separate branch, create a merge request, have that merge request reviewed, then finally merge those changes into the main codebase. Somewhere in the process it would be nice to have some automated testing, but small steps first.

Code reviewing does not have to be long and shouldn't take too much time to write up. It's like sending an essay draft to someone else to peer review it. You can google code review practices or whatever, but the main things you want to be looking out for if you're reviewing someone's code is:
- Is the code consistent with the agreed upon style?
- Is there any glaring errors or redundant / inefficient code?
  - This includes suggestions for improvements.
  - This also includes potential problems the code may pose in the future.
- Asking for clarification.
- Giving complements.

Once you receive the code review, you should address each comment by either:
- Implementing the suggested change / improvement
- Clarify whatever needed clarification.
- Explain why the suggested change is stupid.
- Asking for clarification.

Once the reviewer is happy with the changes / response, they can approve the merge request and (if all reviewers are happy) the changes can be merged in the main codebase.

The previous assignment I told you to mark everyone as reviewers, but not everyone needs to review everyone elses' code. I think at least 1 review should suffice, but this was mainly a practice and an example for the code review process.

Occasionally, there will be some conflicts when merging. We can handle those manually.

### RESTful Requests
As this application we will be making for our final project will be split into a front end React UI and a backend server, everyone needs to know how to make requests to that server. On top of calls to the server, we also plan to utilize APIs from various sources to pull our information from.

Basically, we'll just be making a bunch of [HTTP requests](https://www.w3schools.com/tags/ref_httpmethods.asp) (mainly GET requests) to various places.

A popular package used for making these requests is [Axios](https://www.npmjs.com/package/axios), so I plan to use that as an example. If you know a better alternative to Axios, let us know.

It's important to note that Axios requests are asynchronous, so understand how to handle [promises](https://www.w3schools.com/js/js_promise.asp).

### Binding The Context Of `this`
You'll see that the constructor of some of the components I've created will have some weird lines at the bottom. These exist to bind the context of `this` to those functions.

If we were to pass functions that include `this` within them to other components, the context of `this` when the function gets invoked gets ambiguous and usually causes some confusing runtime errors. Rule of thumb is if your function isn't working, try binding `this` in the component it was declared in and hope for the best.

### Information Sharing Between Components
Take note of how values are being passed between the parent and child components.
- Parents can directly pass a value into a child's props.
- Children **can't** directly pass a value to the parent.

The way we get around the latter point is passing the child a function that will update the state of parent component. This is presented in this assignment through the `incrementTotalRequestsMade()` function in [App.tsx](./src/App.tsx).

## Instructions
1. Make sure you're not working on the main branch.
2. Configure your component to:
   - Make a request to any API of your choosing and obtain some kind of information.
      - Preferably a free API that doesn't require a key.
      - The example I used was chosen randomly in [list of free APIs](https://public-apis.io/free-apis) I found through a quick google search, but you are welcome to use anything you find elsewhere.
   - Display that information in however way you want.
   - Update `totalRequestsMade` in [App.tsx](./src/App.tsx) accordingly.
3. When it works and you're happy, go through the usual merge request and code review process.