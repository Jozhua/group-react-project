# Assignment 1
Basic React stuff.

## Goals For This Assignment
- Learning the basics of React
- Get everyone comfortable with Git and version control.
   - Particularly, working on the same repo and merging changes.

## Getting Started
1. Clone this repository.
2. Create a new branch to work off of.
   - I don't care what you name it.
3. Open the cloned repo folder in the command line or in Visual Studio Code (then hit `Ctrl` + ```\``` to open the terminal) and install all the dependencies by typing `npm i` or `npm install`.
4. Make sure that everything is working correctly by running `npm start`.
   - This should launch a new tab / window for the React project to render onto.

## Instructions
I plan for us to slowly build off of this group project (though I don't really have a final goal for it yet). As such, this first "assignment" can start off small.

1. Within your own branch, create a component in the [components directory](./src/components).
   - I don't care what you name it.
2. Within your component, I want you to render:
   - Your name
   - The current time
   - Whatever else you want. Go wild.
3. Export this component then import it in [App.tsx](./src/App.tsx).
4. Place that component under the where it says to put it.
5. When you're happy with what you have, push your commits then create a merge request.
   - Tag everyone as reviewers.