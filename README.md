# Group React Project
Now that everyone (hopefully) has a better grasp of Node, Javascript / Typescript, etc. we can finally start to work on React.

## Goals For This Project
- Learning the basics of React
- Get everyone comfortable with Git and version control.
   - Particularly, working on the same repo and merging changes.

## Information
This project was created using `npx create-react-app` and I've gone ahead and stripped it of anything unnecessary to reduce confusion. Within it, I've pre-installed typescript, eslint, bootstrap / reactstrap and provided a very basic example of how components can be imported and utilized within other components.

I'm opting for the class-based approach for React, which may be a little out dated due to the popularity of React Redux / Hooks, but I think it's a bit easier to understand from a new-to-react perspective.

Take note of the general structure of the classes. Information passed into the components are outlined by the props interface and values are provided by the parent class. Components can have their own internal state which is also outline by individual the state interfaces.

## Assignments
- [Assignment 1](./Assignment1.md)
- [Assignment 2](./Assignment2.md)